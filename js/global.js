/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function($) {

    Drupal.theme.tableDragChangedWarning = function(name, url) {
        jQuery('.tabledrag-changed-warning').show();
    //return '<div class="tabledrag-changed-warning messages warning">' + Drupal.theme('tableDragChangedMarker') + ' ' + Drupal.t('Changes made in this table will not be saved until the form is submitted.') + '</div>';

    }
    Drupal.behaviors.clickableRow = {
        attach: function (context, settings)
        {
            jQuery('tr.clickable').on('click', function() {
                window.location.href = jQuery(this).attr('data-href');
            });
        }
    }
    Drupal.behaviors.windowResize = {
        attach: function(context, settings)
        {
            var menuNaturalWidth = jQuery('#project_management_user_logged_in_block').width();
            if(jQuery(window).width() < menuNaturalWidth + 400)
            {
                jQuery('#project_management_user_logged_in_block').addClass('icon_only');
            }
            
            jQuery(window).resize(function(){
                
                if(jQuery(window).width() < menuNaturalWidth + 400)
                {
                    jQuery('#project_management_user_logged_in_block').addClass('icon_only');
                } else {
                    jQuery('#project_management_user_logged_in_block').removeClass('icon_only');
                }
            });
        }
    }
})(jQuery);
