<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function project_management_admin_menu_bar_items_form($form, &$form_state)
{
    $menu_weights = project_management_menu_item_weights();   
    
    
    $menus = array_replace_recursive(module_invoke_all('get_menu_items'), $menu_weights);
    uasort($menus, 'drupal_sort_weight');
    //print_r($menus);
    $form['#theme'] = 'project_management_admin_menu_bar_items_form';
    $form['menu_items'] = array(
        '#tree' => true
    );
    foreach ($menus as $menu_index => $menu_item) {

        
    // Create a form entry for this item.
    //
    // Each entry will be an array using the the unique id for that item as
    // the array key, and an array of table row data as the value.
    $form['menu_items'][$menu_index] = array(
      
      // We'll use a form element of type '#markup' to display the item name.
      'name' => array(
        '#markup' => check_plain($menu_item['name']),
      ),
      
      // We'll use a form element of type '#textfield' to display the item
      // description, which will allow the value to be changed via the form.
      // We limit the input to 255 characters, which is the limit we set on
      // the database field.
      /*'description' => array(
        '#type' => 'textfield',
        '#default_value' => check_plain($item->description),
        '#size' => 20,
        '#maxlength' => 255,
      ),*/
      
      // The 'weight' field will be manipulated as we move the items around in
      // the table using the tabledrag activity.  We use the 'weight' element
      // defined in Drupal's Form API.
      'weight' => array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => isset($menu_item['weight']) ? $menu_item['weight'] : 0,
        '#delta' => 100,
        '#title-display' => 'invisible',
      ),
    );
     
     
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Changes'),
  );
  return $form;
    return $form;
}
function project_management_admin_menu_bar_items_form_submit($form, &$form_state)
{
    variable_set('project_management_menu_weights', $form_state['values']['menu_items']);
    drupal_set_message('Menu Item Weights Updated');
}

function theme_project_management_admin_menu_bar_items_form($variables) {
  $form = $variables['form'];
  //print_r($variables);

  // Initialize the variable which will store our table rows.
  $rows = array();

  // Iterate over each element in our $form['example_items'] array.
  foreach (element_children($form['menu_items']) as $id) {

    // Before we add our 'weight' column to the row, we need to give the
    // element a custom class so that it can be identified in the
    // drupal_add_tabledrag call.
    //
    // This could also have been done during the form declaration by adding
    // '#attributes' => array('class' => 'example-item-weight'),
    // directy to the 'weight' element in tabledrag_example_simple_form().
    $form['menu_items'][$id]['weight']['#attributes']['class'] = array('example-item-weight');

    // We are now ready to add each element of our $form data to the $rows
    // array, so that they end up as individual table cells when rendered
    // in the final table.  We run each element through the drupal_render()
    // function to generate the final html markup for that element.
    $rows[] = array(
      'data' => array(
        
        // Add our 'name' column.
        drupal_render($form['menu_items'][$id]['name']),
        
        // Add our 'description' column.
        //drupal_render($form['example_items'][$id]['description']),
        
        // Add our 'weight' column.
        drupal_render($form['menu_items'][$id]['weight']),
      ),
      
      // To support the tabledrag behaviour, we need to assign each row of the
      // table a class attribute of 'draggable'. This will add the 'draggable'
      // class to the <tr> element for that row when the final table is
      // rendered.
      'class' => array('draggable'),
    );
  }

  // We now define the table header values.  Ensure that the 'header' count
  // matches the final column count for your table.
  $header = array(t('Name'), t('Weight'));

  // We also need to pass the drupal_add_tabledrag() function an id which will
  // be used to identify the <table> element containing our tabledrag form.
  // Because an element's 'id' should be unique on a page, make sure the value
  // you select is NOT the same as the form ID used in your form declaration.
  $table_id = 'example-items-table';

  // We can render our tabledrag table for output.
  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));

  // And then render any remaining form elements (such as our submit button).
  $output .= drupal_render_children($form);

  // We now call the drupal_add_tabledrag() function in order to add the
  // tabledrag.js goodness onto our page.
  //
  // For a basic sortable table, we need to pass it:
  // - the $table_id of our <table> element,
  // - the $action to be performed on our form items ('order'),
  // - a string describing where $action should be applied ('siblings'),
  // - and the class of the element containing our 'weight' element.
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'example-item-weight');

  return $output;
}


function project_management_admin_form($form, &$form_state) {
    $default_settings = project_management_default_settings();
    $settings = array_replace_recursive($default_settings, project_management_settings());
    $form['project_base_directory'] = array(
        '#title' => t('Project Base Directory'),
        '#type' => 'textfield',
        '#default_value' => isset($settings['project_base_directory']) ? $settings['project_base_directory'] : ""
    );
    $form['redirect'] = array(
        '#type' => 'checkbox',
        '#title' => t('Redirect if not logged in'),
        
        '#default_value' => $settings['redirect']
    );
    $form['not_logged_in_redirect'] = array(
        '#type' => 'textfield',
        '#title' => t('Not logged in URL'),
        '#description' => t('URL to redirect to if a user is not logged in.'),
        '#default_value' => $settings['not_logged_in_redirect']
    );
    $form['site_base'] = array(
        '#type' => 'textfield',
        '#title' => t('Site Base'),
        '#description' => t('URL to redirect to if a user is not logged in.'),
        '#default_value' => $settings['site_base']
    );
    $form['show_admin_toolbar'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show Administrative Toolbar'),
        '#default_value' => $settings['show_admin_toolbar']
    );
    $form['use_breadcrumb_renderer'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use Breadcrumb Renderer'),
        '#default_value' => $settings['use_breadcrumb_renderer']
    );
    $form['use_complete_dialog'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show Dialog for Task Completion'),
        '#default_value' => $settings['use_complete_dialog']
    );
    $form['use_avatars'] = array(
        '#type' => 'checkbox',
        '#title' => t('Display Avatars'),
        '#default_value' => $settings['use_avatars']
    );
    $form['allow_avatars_for_coordinator'] = array(
        '#type' => 'checkbox',
        '#title' => t('Allow using Avatars for coordinator'),
        '#default_value' => $settings['allow_avatars_for_coordinator']
    );
    $form['submit'] = array(
      '#type' => 'submit',
        '#value' => t('Submit')
    );
    return $form;
}

function project_management_admin_form_submit($form, &$form_state) {
    
    
    $base_dir = $form_state['input']['project_base_directory'];
    $path_parts = explode('/', $base_dir);
    array_filter($path_parts);
    $form_state['input']['path_parts'] = count($path_parts);
    variable_set('project_management_settings', $form_state['input']);
}

?>