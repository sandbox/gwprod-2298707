<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="activity_update">

    <?php
    
    $with_picture = 'with-picture';
    print theme('user_picture', array('account' => $content['#user']));
    ?>
    <div class="activity_update_content <?php echo $with_picture; ?>">
        <?php print $content['#message']; ?>
        <div>
            <?php print $content['#time']; ?>
        </div>
    </div>
</div>