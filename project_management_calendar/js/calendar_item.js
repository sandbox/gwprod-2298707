/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function($) {

    
    Drupal.behaviors.resizeCalendarItem = {
        attach: function (context, settings)
        {
            
            jQuery('div.calendar_item_container').each(function(){
                
                var child_count = jQuery(this).children('div.calendar_item').length;
                var parent_width = 100;
                var offset = 0;
                
                jQuery(this).children('div.calendar_item').each(function(){
                    
                    var new_width = parent_width/child_count;
                    
                    jQuery(this).css({left: offset + '%', width: new_width + '%'});
                    offset += new_width;
                });
            })
        }
    }
})(jQuery);
