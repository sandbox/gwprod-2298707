<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function project_management_calendar_admin_form($form, &$form_state)
{
    $settings = variable_get('project_management_calendar_settings');
    //print_r($settings);
    if(isset($form_state['values']))
    {
        print_r($form_state['values']);
    }
    $form['settings'] = array(
        '#tree' => true
    );
    $form['settings']['menu_prefix'] = array(
        '#title' => t('Menu Prefix'),
        '#type' => 'textfield',
        '#default_value' => isset($settings['menu_prefix']) ? $settings['menu_prefix'] : ''
    );
    $form['save'] = array(
        '#type' => 'submit',
        '#value' => t('Save')
    );
    return $form;
}
function project_management_calendar_admin_form_validate($form, &$form_state)
{
    if(isset($form_state['values']['settings']['menu_prefix']))
    {
        $menu_prefix = $form_state['values']['settings']['menu_prefix'];
        if($menu_prefix[strlen($menu_prefix)-1] != '/')
        {
            form_set_error('settings][menu_prefix', 'The last character of menu prefix must be a forward-slash (/)');
        }
        if($menu_prefix[0] == '/')
        {
            form_set_error('settings][menu_prefix', 'The first character of menu prefix cannot be a forward-slash (/)');
        }
        if(strpos($menu_prefix, '//') !== FALSE)
        {
            form_set_error('settings][menu_prefix', 'Menu prefix cannot contain more than one slash in a row');
        }
    }
}
function project_management_calendar_admin_form_submit($form, &$form_state)
{
    watchdog('project_management_calendar_admin_form_submit', print_r($form_state['values'], TRUE));
    $settings = isset($form_state['values']['settings']) ? $form_state['values']['settings'] : array();
    variable_set('project_management_calendar_settings', $settings);
    menu_rebuild();
}
?>
