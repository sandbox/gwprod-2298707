/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


function drawVisualization() {
var data = new google.visualization.DataTable();
data.addColumn('string', 'Project Name');
data.addColumn('number', 'Lead Time (Days)');
data.addColumn('number', 'Completed (Days)');
data.addColumn('number', 'Remaining (Days)');
data.addRows([
[ 'Project A', 0, {v: 27, f: '27 out of 57 days Completed'}, {v: 30, f: '30 out of 57 days Remaining'} ],
[ 'Project B', 15, {v: 5, f: '5 out of 45 days Completed'}, {v: 40, f: '40 out of 45 days Remaining'} ],
[ 'Project C', 55, {v: 0, f: '0 out of 30 days Completed'}, {v: 30, f: '30 out of 30 days Remaining'} ],
[ 'Project D', 85, {v: 0, f: '0 out of 10 days Completed'}, {v: 10, f: '10 out of 10 days Remaining'} ],
[ 'Project E', 95, {v: 0, f: '0 out of 16 days Completed'}, {v: 16, f: '16 out of 16 days Remaining'} ]
]);
// Create and draw the visualization.
new google.visualization.BarChart(document.getElementById('visualization')).
draw(data,
{title:"Simple Gantt Chart",
width:400, height:200,
series: {0:{visibleInLegend: false}},
isStacked: true,
legend: 'top',
colors: ['transparent', '#149d14', '#c0c0c4'],
hAxis: {title: "Days"}}
);
}
google.setOnLoadCallback(drawVisualization); 