<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function project_management_email_admin_form($form, &$form_state)
{
    $settings = project_management_email_settings();
    $form['project_management_email_settings'] = array(
        '#tree' => true
    );
    $form['project_management_email_settings']['send_email_from_queue'] = array(
        '#type' => 'checkbox',
        '#title' => t('Send Email From Queue'),
        '#description' => t('Email is sent from the queue via cron job'),
        '#default_value' => isset($settings['send_email_from_queue']) ? $settings['send_email_from_queue'] : 0
    );
    
    
    return system_settings_form($form);
}
?>
