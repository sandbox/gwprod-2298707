<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function project_management_event_admin_form($form, &$form_state) {
  $settings = variable_get('project_management_event_settings');


  $form['settings'] = array(
    '#tree' => true
  );
  $form['settings']['redirect'] = array(
    '#type' => 'checkbox',
    '#title' => t('Redirect Edit, Volunteer List and RSVP List?'),
    '#default_value' => isset($settings['redirect']) ? $settings['redirect'] : false
  );
  $form['settings']['redirect_prefix'] = array(
    '#title' => t('Redirect Prefix'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['redirect_prefix']) ? $settings['redirect_prefix'] : '',
    '#description' => t('Redirect paths using this prefix as a base.')
  );
  $form['settings']['drupal_post'] = array(
    '#title' => t('Drupal Post'),
    '#type' => 'fieldset',
    '#tree' => TRUE,
  );
  $form['settings']['drupal_post']['base_url'] = array(
    '#title' => t('Base URL'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['drupal_post']['base_url']) ? $settings['drupal_post']['base_url'] : ''
  );
  $form['settings']['drupal_post']['user_login'] = array(
    '#title' => t('Administrator Login'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['drupal_post']['user_login']) ? $settings['drupal_post']['user_login'] : '',
  );
  $form['settings']['drupal_post']['user_password'] = array(
    '#title' => t('Administrator Password'),
    '#type' => 'password',
    '#description' => t('Saved passwords will not appear in this element.'),
    '#default_value' => isset($settings['drupal_post']['user_password']) ? $settings['drupal_post']['user_password'] : '',
  );
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  return $form;
}

function project_management_calendar_admin_form_validate($form, &$form_state) {
  if (isset($form_state['values']['settings']['redirect_prefix'])) {
    $redirect_prefix = $form_state['values']['settings']['redirect_prefix'];
    if ($redirect_prefix[strlen($menu_prefix) - 1] != '/') {
      form_set_error('settings][menu_prefix', 'The last character of Redirect Prefix must be a forward-slash (/)');
    }
    if ($redirect_prefix[0] == '/') {
      form_set_error('settings][menu_prefix', 'The first character of Redirect Prefix cannot be a forward-slash (/)');
    }
  }
}

function project_management_event_admin_form_submit($form, &$form_state) {

  $settings = isset($form_state['values']['settings']) ? $form_state['values']['settings'] : array();
  variable_set('project_management_event_settings', $settings);
  //menu_rebuild();
}

?>
