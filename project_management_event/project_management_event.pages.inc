<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */




function project_management_event_email_events_form($form, &$form_state, $edit = array())
{
    $upcoming_events = project_management_event_get_upcoming_events();
    $form['events'] = array(
        '#tree' => 'true',
        '#type' => 'fieldset',
        '#title' => t('Events to include in email.')
    );
    foreach($upcoming_events as $event)
    {
        $form['events'][$event->event_id] = array(
            '#tree' => true,
            
        );
        $form['events'][$event->event_id]['title'] = array(
            '#markup' => $event->title
        );
        $form['events'][$event->event_id]['include'] = array(
            '#type' => 'checkbox',
            '#title' => t('Include in Email'),
            '#default_value' => isset($edit['events'][$event->event_id]['include']) ? $edit['events'][$event->event_id]['include'] : 0
        );
        
    }
}
function project_management_event_email_events_form_submit($form, &$form_state)
{
    
}
?>
