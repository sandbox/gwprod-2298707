<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function project_management_event_position_admin_form($form, &$form_state) {
    
    $form['automatic_positions'] = array(
        '#tree' => true,
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#title' => t('Automatic Positions')
    );
    $settings = variable_get('project_management_event_position_settings');
    //print_r($settings);
    /* if (isset($settings['automatic_positions_by_role'])) {
      foreach ($settings['automatic_positions_by_role'] as $role) {

      }
      } */
    
    //print_r($tokens);
    //print_r($data);
    $form['email_templates'] = array(
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#title' => t('Email Templates'),
        '#weight' => -100,
        '#tree' => true
    );

    $form['email_templates']['signup_email'] = array(
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#title' => t('Signup Email'),
        '#tree' => true
    );
    $form['email_templates']['signup_email']['send_email'] = array(
        '#type' => 'checkbox',
        '#title' => t('Send Email Confirmation on Signup'),
        '#default_value' => isset($settings['email_templates']['signup_email']['send_email']) ? $settings['email_templates']['signup_email']['send_email'] : 0
    );
    $form['email_templates']['signup_email']['email_subject'] = array(
        '#type' => 'textfield',
        '#title' => t('Email Subject Field'),
        '#default_value' => isset($settings['email_templates']['signup_email']['email_subject']) ? $settings['email_templates']['signup_email']['email_subject'] : '',
        );
    $form['email_templates']['signup_email']['email_template'] = array(
        '#type' => 'text_format',
        '#title' => t('Email Template'),
        '#default_value' => isset($settings['email_templates']['signup_email']['email_template']) ? $settings['email_templates']['signup_email']['email_template']['value'] : '',
        '#format' => isset($settings['email_templates']['signup_email']['email_template']) ? $settings['email_templates']['signup_email']['email_template']['format'] : 'full_html'
    );
    $form['email_templates']['signup_email']['tokens'] = array(
        '#theme' => 'token_tree',
        '#token_types' => array('user', 'project_management_event_position'), // The token types that have specific context. Can be multiple token types like 'term' and/or 'user'
        '#global_types' => TRUE, // A boolean TRUE or FALSE whether to include 'global' context tokens like [current-user:*] or [site:*]. Defaults to TRUE.
        '#click_insert' => TRUE, // A boolean whether to include the 'Click this token to insert in into the the focused textfield' JavaScript functionality. Defaults to TRUE.
    );
    $form['email_templates']['reminder_email'] = array(
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#title' => t('Reminder Email'),
        '#tree' => true
    );
    $form['email_templates']['reminder_email']['send_email'] = array(
        '#type' => 'checkbox',
        '#title' => t('Send Email Confirmation on Signup'),
        '#default_value' => isset($settings['email_templates']['reminder_email']['send_email']) ? $settings['email_templates']['reminder_email']['send_email'] : 0
    );
    $form['email_templates']['reminder_email']['email_subject'] = array(
        '#type' => 'textfield',
        '#title' => t('Email Subject Field'),
        '#default_value' => isset($settings['email_templates']['reminder_email']['email_subject']) ? $settings['email_templates']['reminder_email']['email_subject'] : '',
        );
    $form['email_templates']['reminder_email']['email_template'] = array(
        '#type' => 'text_format',
        '#title' => t('Email Template'),
        '#default_value' => isset($settings['email_templates']['reminder_email']['email_template']) ? $settings['email_templates']['reminder_email']['email_template']['value'] : '',
        '#format' => isset($settings['email_templates']['reminder_email']['email_template']) ? $settings['email_templates']['reminder_email']['email_template']['format'] : 'full_html'
    );
    $form['email_templates']['reminder_email']['tokens'] = array(
        '#theme' => 'token_tree',
        '#token_types' => array('user', 'project_management_event_position'), // The token types that have specific context. Can be multiple token types like 'term' and/or 'user'
        '#global_types' => TRUE, // A boolean TRUE or FALSE whether to include 'global' context tokens like [current-user:*] or [site:*]. Defaults to TRUE.
        '#click_insert' => TRUE, // A boolean whether to include the 'Click this token to insert in into the the focused textfield' JavaScript functionality. Defaults to TRUE.
    );
    
    $form['email_templates']['confirmed_email'] = array(
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#title' => t('Confirmed Email'),
        '#tree' => true
    );
    $form['email_templates']['confirmed_email']['send_email'] = array(
        '#type' => 'checkbox',
        '#title' => t('Send Email Confirmation on Signup'),
        '#default_value' => isset($settings['email_templates']['confirmed_email']['send_email']) ? $settings['email_templates']['confirmed_email']['send_email'] : 0
    );
    $form['email_templates']['confirmed_email']['email_subject'] = array(
        '#type' => 'textfield',
        '#title' => t('Email Subject Field'),
        '#default_value' => isset($settings['email_templates']['confirmed_email']['email_subject']) ? $settings['email_templates']['confirmed_email']['email_subject'] : '',
        );
    $form['email_templates']['confirmed_email']['email_template'] = array(
        '#type' => 'text_format',
        '#title' => t('Email Template'),
        '#default_value' => isset($settings['email_templates']['confirmed_email']['email_template']) ? $settings['email_templates']['confirmed_email']['email_template']['value'] : '',
        '#format' => isset($settings['email_templates']['confirmed_email']['email_template']) ? $settings['email_templates']['confirmed_email']['email_template']['format'] : 'full_html'
    );
    $form['email_templates']['confirmed_email']['tokens'] = array(
        '#theme' => 'token_tree',
        '#token_types' => array('user', 'project_management_event_position'), // The token types that have specific context. Can be multiple token types like 'term' and/or 'user'
        '#global_types' => TRUE, // A boolean TRUE or FALSE whether to include 'global' context tokens like [current-user:*] or [site:*]. Defaults to TRUE.
        '#click_insert' => TRUE, // A boolean whether to include the 'Click this token to insert in into the the focused textfield' JavaScript functionality. Defaults to TRUE.
    );
    $roles = user_roles(TRUE);
    foreach ($roles as $role_id => $role_name) {
        $form['automatic_positions'][$role_id] = array(
            '#type' => 'fieldset',
            '#title' => check_plain(t('Automatic Positions for Role: ' . $role_name)),
            '#weight' => 5,
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
        );
        $form['automatic_positions'][$role_id]['positions'] = array(
            '#title' => t('Positions'),
            '#type' => 'textfield',
            '#default_value' => isset($settings['automatic_positions'][$role_id]['positions']) ? implode(',', $settings['automatic_positions'][$role_id]['positions']) : '',
            '#description' => t('Enter a comma separated list of position ids')
        );
    }
    $form['save'] = array(
        '#type' => 'submit',
        '#value' => t('Save')
    );
    return $form;
}

function project_management_event_position_admin_form_submit($form, &$form_state) {
    //$save_array = array();
    //$save_array['automatic_positions'];
    //watchdog('project_management_event_position_admin_form_submit', print_r($form_state['values'], TRUE));
    $save_array = $form_state['values'];
    unset($save_array['automatic_positions']);
    foreach ($form_state['values']['automatic_positions'] as $role_id => $role_positions) {
        $position_ids = explode(',', $role_positions['positions']);
        foreach ($position_ids as $position_id) {
            $trimmed_position_id = trim($position_id);
            if (is_numeric($trimmed_position_id)) {
                $save_array['automatic_positions'][$role_id]['positions'][] = $trimmed_position_id;
            }
        }
    }
    variable_set('project_management_event_position_settings', $save_array);
}

?>
