<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$uri = $content['position']->uri();
?>
<div class="calendar_item position_week_view" style="height:<?php print $content['height'];?>px;top:<?php print $content['top_offset'];?>px;">
  
  <a href="<?php print url($uri['path']); ?>">
    <?php print $content['position']->label(); ?>
    
  </a>
  <span class="times">
    <?php print $content['start_time']; ?> to <?php print $content['end_time']; ?>
  </span>
</div>