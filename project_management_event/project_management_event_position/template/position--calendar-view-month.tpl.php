<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$uri = $content['position']->uri();
?>
<div class="position_month_view">
  <div><strong><em>Volunteer Opportunity</em></strong></div>
  <a href="<?php print url($uri['path']); ?>">
    <?php print $content['position']->label(); ?>
  </a>
  <span class="times">
    <?php print $content['start_time']; ?> to <?php print $content['end_time']; ?>
  </span>
</div>