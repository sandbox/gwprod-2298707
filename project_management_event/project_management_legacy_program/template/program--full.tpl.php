<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  
    

  <div class="content"<?php print $content_attributes; ?>>
      <h2><?php print $content['title'];?></h2>
      <p><?php print $content['description'];?></p>
    <?php
      
      print render($content);
      
    ?>
      
  </div>
</div>