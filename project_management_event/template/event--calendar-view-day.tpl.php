<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$uri = $content['event']->uri();
?>
<div class="calendar_item event_week_view" style="height:<?php print $content['height'];?>px;">
  
  <a href="<?php print url($uri['path']); ?>">
    <?php print $content['event']->label(); ?>
    
  </a>
  <span class="times">
    <?php print $content['start_time']; ?> to <?php print $content['end_time']; ?>
  </span>
</div>