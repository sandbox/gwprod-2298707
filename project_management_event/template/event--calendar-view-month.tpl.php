<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$uri = $content['event']->uri();
?>
<div class="event_month_view">
  <div><strong><em>Event</em></strong></div>
  <a href="<?php print url($uri['path']); ?>">
    <?php print $content['event']->label(); ?>
  </a>
  <span class="times">
    <?php print $content['start_time']; ?> to <?php print $content['end_time']; ?>
  </span>
</div>