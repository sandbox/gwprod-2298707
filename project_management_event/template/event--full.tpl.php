<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="event full">
    <?php print render($content);?>
    
    <div class="event_title"> 
        <h2><?php print $content['#object']->title; ?></h2>
    </div>

    <div class="event_description"> 
        <span class="label above">Event Description</span>
        <span class="event_description"><?php print render($content['description']); ?></span>
    </div>
    <div class="event_date"> 
        <span class="label above">Event Date</span>
        <span class="event_date"><?php print $content['time_span']; ?></span>
    </div>
    <div class="event_coordinators"> 
        <span class="label above">Event Coordinators</span>
        <span class="event_coordinators"><?php print $content['coordinators']; ?></span>
    </div>

    <?php if ($content['participation_event'] > 0): ?>
        <div class="participation">
            <span class="label">This event requires an RSVP</span>
            <div id="participation_block">
                <?php print $content['participation_block']; ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($content['volunteer_event'] && isset($content['positions'])): ?>

        <div class="positions">

            <?php print $content['positions']; ?>

        </div>
    <?php endif; ?>
</div>