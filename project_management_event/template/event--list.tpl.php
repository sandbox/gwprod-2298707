<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="event full">
    <div class="event_title"> 
        <h2><?php print l($content['#object']->title, $content['url']); ?></h2>
    </div>
    <div class="event_date"> 
        <span class="label above">Event Date</span>
        <span class="event_date"><?php print $content['time_span']; ?></span>
    </div>
    <div class="event_description"> 

        <span class="event_description"><?php print nl2br($content['#object']->description, TRUE); ?></span>
    </div>

    <?php if ($content['participation_event'] && $content['volunteer_event']): ?>

        <?php print l('Click here to volunteer or attend', $content['url']); ?>
    <?php else: ?>
        <?php if ($content['participation_event']): ?>

            <?php print l('Click here to attend', $content['url']); ?>
        <?php endif; ?>
        <?php if ($content['volunteer_event']): ?>

            <?php print l('Click here to volunteer', $content['url']); ?>
        <?php endif; ?>
    <?php endif; ?>



</div>