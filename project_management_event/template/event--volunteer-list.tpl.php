<?php
/**
 * @file
 * Default print module template
 *
 * @ingroup print
 */
//watchdog('print_template', print_r($content, TRUE));
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Untitled Document</title>
        <style>
            * {
                margin: 0;
                padding: 0;
            }
            html {
                margin: 0.25in !important;
                padding: 0 !important;
            }
            html, body {
                background: none repeat scroll 0 0 #FFFFFF;
                font-size: 12px;
                font-weight: bold;
                height: 100%;
                width: 8in;
            }
            table.volunteer-list {
                border: 1px solid black;
                border-collapse: collapse;
                width: 100%;
            }
            th {
                background-color: lightGray;
                border: 1px solid black;
                padding:5px 5px 5px 5px;
                text-align: left;
            }
            table.volunteer-list td {
                border-left: 1px solid black;
            }
            td.td_offset {
                max-width: 45px;
                width: 45px;
            }

            table.volunteer-list td.user_repeat {
                border-bottom: 0 none;
                border-top: 0 none;
            }
            td {
                border-bottom: 1px solid black;
            }
            td.signature_area {
                border-left: 1px solid black;
                border-right: 1px solid black;
                width: 250px;
            }
            table.volunteer-list td {
                font-size: 11px;
                font-weight: bold;
                padding: 10px;
            }
            .rowgroup_header {
                background-color: lightGray;
                color: black;
            }
            tr {
            }
            .template {
                left: 0;
                top: 0;
                width: auto;
            }
            img {
                width: 100%;
            }
            li {
                margin-bottom: 5px !important;
                margin-left: 20px !important;
                margin-top: 5px !important;
            }
            .activity_name {
                display: inline-block;
                width: 5in;
            }
            .date {
                display: inline-block;
                width: 2in;
            }
            .coordinators {
                display: block;
                width: 7in;
            }
            .header {
                border-bottom: 1px solid #000000;
                height: 1.2in;
                margin: 0;
                padding: 0;
            }
            .footer {
            }
            .budget {
                display: inline-block;
                width: 2in;
            }
            .participants {
                display: inline-block;
                width: 3in;
            }
            .hours_served {
                display: inline-block;
                width: 3in;
            }
            .content {
                height: 8in;
                margin: 0;
                padding: 0;
            }
            .content .items {
                padding-top: 10px !important;
                text-decoration: underline;
            }

            table.volunteer-list td.user_first {
                border-bottom: 0 none;
                border-top: 1px solid black;
                vertical-align: top;
            }
        </style>
    </head>

    <body>
        <?php print render($content['header']);?>
        <?php print render($content['volunteer_list']); ?>
    </body>
</html>