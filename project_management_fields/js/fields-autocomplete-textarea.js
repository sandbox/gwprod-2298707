/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


(function($) {

    var rebuildId = 0;
    var ajaxId = 0;
    var caret_position = 0;
    Drupal.behaviors.autocompleteTextarea = {
        attach: function(context, settings)
        {
            
            jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.textarea_id).on('click', function(event) {
                var suggestions = jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.suggestions_id);
                suggestions.hide();
            });
            jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.textarea_id).on('paste', function(event)
            {
                var data = event.originalEvent.clipboardData.getData('Text');
                
                rebuildDescription(Drupal.settings.project_management_fields.textarea_autocomplete.mentions_data_id);
            });
            jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.textarea_id).keyup(function(event) {
                if (event.which == 8 || event.which == 32) {
                    var suggestions = jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.suggestions_id);
                    suggestions.hide();
                    clearTimeout(rebuildId); // doesn't matter if it's 0
                    
                    rebuildId = setTimeout(function() {
                        
                        rebuildDescription(Drupal.settings.project_management_fields.textarea_autocomplete.mentions_data_id);
                        ;
                    }, 1);

                }
            });
            jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.textarea_id).keypress(function(event) {

                if (event.which == 8 || event.which == 32) {
                    var suggestions = jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.suggestions_id);
                    suggestions.hide();
                    clearTimeout(rebuildId); // doesn't matter if it's 0

                    rebuildId = setTimeout(function() {

                        rebuildDescription(Drupal.settings.project_management_fields.textarea_autocomplete.mentions_data_id);
                        ;
                    }, 1);

                } else if (event.which != 0 && event.which != 32 && event.which != 9 && event.which != 13) {
                    
                    clearTimeout(ajaxId); // doesn't matter if it's 0

                    ajaxId = setTimeout(function() {
                        autocomplete(Drupal.settings.project_management_fields.textarea_autocomplete.textarea_id, event);
                    }, 500);
                    clearTimeout(rebuildId); // doesn't matter if it's 0
                    rebuildId = setTimeout(function() {
                        rebuildDescription(Drupal.settings.project_management_fields.textarea_autocomplete.mentions_data_id);
                        ;
                    }, 1);
                }
            });
            jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.textarea_id).keydown(function(event) {

            });


        }
    }
    Drupal.behaviors.autocompleteSuggestion = {
        attach: function(context, settings)
        {
            jQuery('li.suggestion').on('click', function(event) {
                var suggestions = jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.suggestions_id);
                var data = jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.textarea_id).val();
                var object = jQuery(this).attr('id');
                var object_text = object.substring(1, object.length - 1);
                var parts = object_text.split(":");
                jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.textarea_id).val(rebuildText(data, parts[1]));
                addMentions(Drupal.settings.project_management_fields.textarea_autocomplete.mentions_data_id, object_text);
                rebuildDescription(Drupal.settings.project_management_fields.textarea_autocomplete.mentions_data_id);
                suggestions.hide();
                jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.textarea_id).focus();
                //jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.textarea_id).caret(caret_position, caret_position);
            });
        }
    }
})(jQuery)
function autocomplete(selector, event)
{

    caret_position = jQuery(selector).caret().start;
    
    var data = jQuery(selector).val();
    if (data.indexOf('@') != -1)
    {
        caret_position = jQuery(selector).caret().start;
        var search_string = data.substring(data.indexOf('@') + 1, caret_position).trim();
        
        jQuery.ajax({
            type: "POST",
            dataType: "json",
            url: Drupal.settings.project_management_fields.textarea_autocomplete.autocomplete_path,
            data: {
                data: search_string
            }
        }).done(function(data) {
            
            var suggestions = jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.suggestions_id);

            if (data.length !== 0)
            {

                suggestions.show();
                suggestions.html(translateJSONResults(data));
                Drupal.attachBehaviors();
            } else {
                suggestions.hide();
            }
        });
    } else {
        //data += String.fromCharCode(event.which);
        var last_word = parseText(data, caret_position);
        jQuery.ajax({
            type: "POST",
            dataType: "json",
            url: Drupal.settings.project_management_fields.textarea_autocomplete.autocomplete_path,
            data: {
                data: last_word
            }
        }).done(function(data) {
            
            var suggestions = jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.suggestions_id);

            if (data.length !== 0)
            {

                suggestions.show();
                suggestions.html(translateJSONResults(data));
                Drupal.attachBehaviors();
            } else {
                suggestions.hide();
            }
        });
    }
}
function parseText(text, caret_position)
{
    var portion_recently_typed = text.substring(0, caret_position);
    var words = portion_recently_typed.trim().split(" ");
    var last_word = words[words.length - 1];

    
    return last_word;
}


function rebuildText(text, object)
{
    var words = text.split(" ");
    var object_words = object.split(" ");
    var insert_index = words.length - 1;
    var replace_how_many = 0;
    if (text.indexOf('@') != -1)
    {
        //caret_position = jQuery(selector).caret().start;
        var after = text.substring(caret_position);
        
        var before = text.substring(0, text.indexOf('@'));
        
        text = before + object + ' ' + after.trim();
    } else {
        for (var i = words.length - 1; i >= 0; i--)
        {
            if (object_words[0].toLowerCase().indexOf(words[i].toLowerCase()) == 0)
            {
                
                insert_index = i;
                replace_how_many++;
                for (var j = 1; j <= object_words.length - 1; j++)
                {
                    if (typeof words[insert_index + j] != 'undefined')
                    {
                        
                        if (object_words[j].toLowerCase().indexOf(words[insert_index + j].toLowerCase()) == 0)
                        {
                            
                            replace_how_many++;
                        }
                    }
                }
                break;
            }
        }
        words.splice(insert_index, replace_how_many, object);
        
        //words[words.length - 1] = object;
        text = words.join(' ');
    }

    return text;
}
function attachAutocomplete(selector, data)
{

}

function translateJSONResults(data)
{
    var output = '<ul class="suggestions">';

    jQuery.each(data, function(key, val) {
        output += '<li class="suggestion" id="' + key + '"><span class="name">' + val.name + '</span>';
        if (typeof val.partner_organization != 'undefined') {
            output += '<span class="organization">' + val.partner_organization + '</span>'
        }

        output += '</li>';
    });
    output += '</ul>';
    return output;
}
function updateMentions(mentions_selector, mentions)
{
    //var mentions_items = jQuery.data(mentions_selector, 'mentions');
    jQuery(mentions_selector).val(JSON.stringify(mentions));
}

function addMentions(mentions_selector, mention_string)
{
    var mention_parts = mention_string.split(':');
    mention = new Object();
    mention.key = mention_parts[0];
    mention.value = mention_parts[1];
    var mentions = getMentions(mentions_selector);
    mentions.push(mention);
    updateMentions(mentions_selector, mentions);
}
function getMentions(mentions_selector)
{
    var mentions = jQuery(mentions_selector).val();
    
    var mentions_items = jQuery.parseJSON(mentions);
    
    mentions_items.clean(undefined);
    return mentions_items;
}

function rebuildDescription(mentions_selector)
{
    var highlighter = jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.textarea_id).val();
    var mentions_description = jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.textarea_id).val();
    var mentions = getMentions(mentions_selector);
    
    var working_mentions = mentions;
    if (jQuery.isArray(working_mentions) && working_mentions[0] != null)
    {

        jQuery.each(working_mentions, function(key, object) {
            
            if (typeof object != 'undefined' && highlighter.indexOf(object.value) !== -1)
            {
                highlighter = highlighter.replace(object.value, '<b>' + object.value + '</b>');
                mentions_description = mentions_description.replace(object.value, '@[' + object.key + ']');
            } else {
                
                delete mentions[key];
            }
        });
    }
    jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.mentions_id).val(mentions_description);
    jQuery(Drupal.settings.project_management_fields.textarea_autocomplete.highlighter_id).html(highlighter);
    updateMentions(mentions_selector, mentions);
}


Array.prototype.clean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};

