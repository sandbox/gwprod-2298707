/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


(function ($) {
    
    Drupal.behaviors.date_time = {
        attach: function (context, settings)
        {
            $('.form-date-time-element').timepicker({ 
                'timeFormat': 'h:i A',
                'step' : 5,
                'forceRoundTime': true
            });
        }
    }
})(jQuery)