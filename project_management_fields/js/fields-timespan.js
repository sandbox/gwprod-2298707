/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


(function ($) {
    
    Drupal.behaviors.timespan = {
        attach: function (context, settings)
        {
            $('.form-timespan-element').timepicker({ 
                'timeFormat': 'h:i A',
                'step' : 5,
                'forceRoundTime': true
            });
            
        }
    }
})(jQuery)