/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    
    Drupal.behaviors.tooltip = {
        attach: function (context, settings)
        {
            
            jQuery(document).tooltip({
                items: ".tooltip",
                content: function() {
                    

                    var tip = jQuery(this).next('.tooltip_text').html();

                    return tip;
                }
                
            });
        }
    }
})(jQuery)
