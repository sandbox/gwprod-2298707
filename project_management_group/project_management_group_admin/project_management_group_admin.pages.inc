<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function project_management_group_admin_user_list($group)
{
    global $user;
    if (isset($task)) {
        $entity = $task;
    } else {
        $entity = $project;
    }
    $base_dir = project_management_base_dir();
    drupal_add_library('system', 'drupal.ajax');
    drupal_add_library('system', 'ui.draggable');
    drupal_add_library('system', 'ui.droppable');
    drupal_add_library('system', 'ui.sortable');
    drupal_add_js(drupal_get_path('module', 'project_management_user') . '/js/project_management_user.js');
    /* drupal_add_js(array('project_management_user' => array('user_URL' => '/project/' . $base_dir . '/' . $entity->project_id . '/users/assign_users')), 'setting');
      drupal_add_js(array('project_management_user' => array('user_remove_URL' => '/project/' . $base_dir . '/' . $entity->project_id . '/users/remove')), 'setting');
     */
    $output = '';
    drupal_add_js(array('project_management_user' => array('logged_in_user_id' => $user->uid)), 'setting');
    if ($entity->type == 'project') {
        drupal_add_js(array('project_management_user' => array('user_URL' => '/project/' . $base_dir . '/' . $entity->project_id . '/users/assign_users')), 'setting');
        drupal_add_js(array('project_management_user' => array('user_remove_URL' => '/project/' . $base_dir . '/' . $entity->project_id . '/users/remove')), 'setting');

        drupal_set_title('Project Users');
    } else {
        drupal_add_js(array('project_management_user' => array('user_URL' => '/project/' . $base_dir . '/' . $entity->project_id . '/objective/' . $entity->objective_id . '/task/' . $entity->task_id . '/users/assign_users')), 'setting');
        drupal_add_js(array('project_management_user' => array('user_remove_URL' => '/project/' . $base_dir . '/' . $entity->project_id . '/objective/' . $entity->objective_id . '/task/' . $entity->task_id . '/users/remove')), 'setting');

        drupal_set_title('Task Users');
    }
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'user_membership')
            ->propertyCondition('entity_id', $entity->identifier())
            ->propertyCondition('entity_type', $entity->type)
            ->propertyOrderBy('approved', 'DESC')
            ->execute();
    $rows = array();
    $uids = array();
    $approved = array();
    $unapproved = array();
    $coordinators = array();
    if (isset($result['user_membership'])) {


        $users = project_management_user_membership_load_multiple(array_keys($result['user_membership']));
        if (!empty($users)) {
            foreach ($users as $account) {
                $uids[] = $account->uid;
                $view = project_management_user_membership_entity_view($account, 'list');
                if ($entity->type == 'project') {
                    if ($account->approved == 1) {
                        if ($account->administrator == 1
                        ) {
                            $coordinators[] = array(
                                'data' => drupal_render($view),
                                'id' => 'user_' . $account->uid
                            );
                        } else {
                            $approved[] = array(
                                'data' => drupal_render($view),
                                'id' => 'user_' . $account->uid
                            );
                        }
                    } else {
                        $unapproved[] = array(
                            'data' => drupal_render($view),
                            'id' => 'user_' . $account->uid
                        );
                    }
                } else {
                    $approved[] = array(
                        'data' => drupal_render($view),
                        'id' => 'user_' . $account->uid
                    );
                }
            }
        }
    }
    /*
     * #user_list, #available_list_div {
      display:inline-block;
      float:left;
      margin-right:100px;
      min-height:100px;
      }
     */
    $approved[] = array(
        'data' => ''
    );
    $unapproved[] = array(
        'data' => ''
    );
    $coordinators[] = array(
        'data' => ''
    );
    if ($entity->type == 'project') {
        $content['coordinator_list'] = array(
            '#theme' => 'item_list',
            '#items' => $coordinators,
            '#type' => 'ul',
            '#title' => 'Coordinators',
            '#attributes' => array('id' => 'coordinator_list', 'class' => 'connectedSortable'),
            '#weight' => -50
        );

        $content['unapproved_list'] = array(
            '#theme' => 'item_list',
            '#items' => $unapproved,
            '#type' => 'ul',
            '#title' => 'Waiting to be approved',
            '#attributes' => array('id' => 'unapproved_list', 'class' => 'sortable'),
            '#weight' => -48
        );
    }
    $content['approved_list'] = array(
        '#theme' => 'item_list',
        '#items' => $approved,
        '#type' => 'ul',
        '#title' => 'Approved',
        '#attributes' => array('id' => 'approved_list', 'class' => 'connectedSortable'),
        '#weight' => -48
    );
    $content['remove_confirm_dialog'] = array(
        '#markup' => '
            <div id="dialog-confirm-remove" class="dialog-confirm" title="Remove yourself from coordinators?">
                <p>
                   <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;">
                   </span>
                   This will remove you from the project.  Are you sure you wish to do this?
                   </p>
            </div>'
    );
    $content['demote_confirm_dialog'] = array(
        '#markup' => '
            <div id="dialog-confirm-demote" class="dialog-confirm" title="Demote yourself from coordinator role?">
                <p>
                   <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;">
                   </span>
                   This will demote you from coordinator on this project.  Are you sure you wish to do this?
                   </p>
            </div>'
    );
    $content['#weight'] = 90;
    $output .= '<div id="user_list">' . drupal_render($content) . '</div>';
    if ($entity->type == 'project') {
        $output .= '<div id="available_list_div">' . drupal_render(drupal_get_form('project_management_user_available_user_list_form', $entity, $uids)) . '</div>';
    } else {
        $output .= '<div id="available_list_div">' . drupal_render(project_management_user_available_user_list($search_term = null, $entity, $uids)) . '</div>';
    }
    return $output;
}
?>
