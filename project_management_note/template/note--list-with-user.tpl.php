<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//print 'something';
?>
<div class="note">
    <span class ="user_content">
        <?php
        print theme('user_picture', array('account' => $content['#user']));
        $with_picture = 'with-picture';
        print theme('username', array('account' => $content['#user']));
        ?>
    </span>
    <div class="note_content">

        <div class="message_content"><?php print $content['#object']->description(); ?></div>
        <p class="message_footer"><?php print date('m-d-Y \a\t h:m a', $content['#object']->created); ?>
    </div>
</div>