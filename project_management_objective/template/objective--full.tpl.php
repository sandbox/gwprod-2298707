<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//print_r($content);
?>
<div class ='task-details'>
    <div class="objective_full_description">
        <h3 class="field-label">Objective Description</h3>
        <?php print nl2br($content['description']);?>
    </div>
    <div class="deadline">
        <?php print render($content['field_deadline']);?>
    </div>
    <div class="task_list">
        <?php print render($content['tasks']);?>
    </div>
</div>