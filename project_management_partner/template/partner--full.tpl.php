<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>




    <div class="content"<?php print $content_attributes; ?>>
        <?php
            print render($content);
        ?>
        <div id="partner_notes">
            <?php print render($content['#notes']); ?>
        </div>
        <div id="partner_projects">

            <?php print render($content['#projects']); ?>
        </div>
    </div>
</div>