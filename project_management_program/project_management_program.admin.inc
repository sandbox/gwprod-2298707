<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function project_management_program_admin_form($form, &$form_state)
{
  $settings = project_management_program_settings();
  $form['settings']['display_most_recent_project_event'] = array(
    '#title' => t('Display the most recent Program Instance (Most Recent Project)'),
    '#type' => 'checkbox',
    '#default_value' => isset($settings['display_most_recent_project_event']) ? $settings['display_most_recent_project_event'] : ''
  );
  
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
}
function project_management_program_admin_form_submit($form, &$form_state)
{
  variable_set('project_management_program_settings', $form_state['values']['settings']);
  drupal_set_message('Project Management Program Settings Saved');
}

?>
