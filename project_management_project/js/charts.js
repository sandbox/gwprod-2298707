/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
// Load the Visualization API and the piechart package.
google.load('visualization', '1.0', {
    'packages':['linechart']
    });

// Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawVisualization);

function drawVisualization() {
    var data = new google.visualization.DataTable();
    data.addColumn('date', 'Date');
    data.addColumn('number', 'Sold Pencils');
    data.addColumn('string', 'title1');
    data.addColumn('string', 'text1');
    data.addColumn('number', 'Sold Pens');
    data.addColumn('string', 'title2');
    data.addColumn('string', 'text2');
    data.addRows([
        [new Date(2008, 1 ,1), 30000, null, null, 40645, null, null],
        [new Date(2008, 1 ,2), 14045, null, null, 20374, null, null],
        [new Date(2008, 1 ,3), 55022, null, null, 50766, null, null],
        [new Date(2008, 1 ,4), 75284, null, null, 14334, 'Out of Stock', 'Ran out of stock on pens at 4pm'],
        [new Date(2008, 1 ,5), 41476, 'Bought Pens', 'Bought 200k pens', 66467, null, null],
        [new Date(2008, 1 ,6), 33322, null, null, 39463, null, null],
        [new Date(2008, 1 ,7), 30000, null, null, 40645, null, null],
        [new Date(2008, 1 ,8), 14045, null, null, 20374, null, null],
        [new Date(2008, 1 ,9), 55022, null, null, 50766, null, null],
        [new Date(2008, 1 ,10), 75284, null, null, 14334, 'Out of Stock', 'Ran out of stock on pens at 4pm'],
        [new Date(2008, 1 ,11), 41476, 'Bought Pens', 'Bought 200k pens', 66467, null, null],
        [new Date(2008, 1 ,12), 33322, null, null, 39463, null, null],
        [new Date(2008, 1 ,13), 30000, null, null, 40645, null, null],
        [new Date(2008, 1 ,14), 14045, null, null, 20374, null, null],
        [new Date(2008, 1 ,15), 55022, null, null, 50766, null, null],
        [new Date(2008, 1 ,16), 75284, null, null, 14334, 'Out of Stock', 'Ran out of stock on pens at 4pm'],
        [new Date(2008, 1 ,17), 41476, 'Bought Pens', 'Bought 200k pens', 66467, null, null],
        [new Date(2008, 1 ,18), 33322, null, null, 39463, null, null],
        [new Date(2008, 1 ,19), 30000, null, null, 40645, null, null],
        [new Date(2008, 1 ,20), 14045, null, null, 20374, null, null],
        [new Date(2008, 1 ,21), 55022, null, null, 50766, null, null],
        [new Date(2008, 1 ,22), 75284, null, null, 14334, 'Out of Stock', 'Ran out of stock on pens at 4pm'],
        [new Date(2008, 1 ,23), 41476, 'Bought Pens', 'Bought 200k pens', 66467, null, null],
        [new Date(2008, 1 ,24), 33322, null, null, 39463, null, null],
        [new Date(2008, 1 ,25), 30000, null, null, 40645, null, null],
        [new Date(2008, 1 ,26), 14045, null, null, 20374, null, null],
        [new Date(2008, 1 ,27), 55022, null, null, 50766, null, null],
        [new Date(2008, 1 ,28), 75284, null, null, 14334, 'Out of Stock', 'Ran out of stock on pens at 4pm'],
        [new Date(2008, 2 ,1), 41476, 'Bought Pens', 'Bought 200k pens', 66467, null, null],
        [new Date(2008, 2 ,2), 33322, null, null, 39463, null, null],
        [new Date(2008, 2 ,3), 30000, null, null, 40645, null, null],
        [new Date(2008, 2 ,8), 14045, null, null, 20374, null, null],
        [new Date(2008, 2 ,9), 55022, null, null, 50766, null, null],
        [new Date(2008, 2 ,10), 75284, null, null, 14334, 'Out of Stock', 'Ran out of stock on pens at 4pm'],
        [new Date(2008, 2 ,11), 41476, 'Bought Pens', 'Bought 200k pens', 66467, null, null],
        [new Date(2008, 2 ,12), 33322, null, null, 39463, null, null],
        [new Date(2008, 2 ,13), 30000, null, null, 40645, null, null],
        [new Date(2008, 2 ,14), 14045, null, null, 20374, null, null],
        [new Date(2008, 2 ,15), 55022, null, null, 50766, null, null],
        [new Date(2008, 2 ,16), 75284, null, null, 14334, 'Out of Stock', 'Ran out of stock on pens at 4pm'],
        [new Date(2008, 2 ,17), 41476, 'Bought Pens', 'Bought 200k pens', 66467, null, null],
        [new Date(2008, 2 ,18), 33322, null, null, 39463, null, null],
        [new Date(2008, 2 ,19), 30000, null, null, 40645, null, null],
        [new Date(2008, 2 ,20), 14045, null, null, 20374, null, null],
        [new Date(2008, 2 ,21), 55022, null, null, 50766, null, null],
        [new Date(2008, 2 ,22), 75284, null, null, 14334, 'Out of Stock', 'Ran out of stock on pens at 4pm'],
        [new Date(2008, 2 ,23), 41476, 'Bought Pens', 'Bought 200k pens', 66467, null, null],
        [new Date(2008, 2 ,24), 33322, null, null, 39463, null, null]
        ]);

    //var annotatedtimeline = new google.visualization.AnnotatedTimeLine(document.getElementById('visualization'));
    //annotatedtimeline.draw(data, {    'displayAnnotations': false});
    new google.visualization.LineChart(document.getElementById('visualization')).
      draw(data, {curveType: "function",
                  width: 500, height: 400,
                  vAxis: {maxValue: 10}}
          );
}