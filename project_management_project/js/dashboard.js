/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


(function($) {
    Drupal.behaviors.dashboardMasonry = {
        attach: function(context, settings) {
            jQuery(function() {
                jQuery('#dashboard-content').masonry({
                    // options
                    itemSelector: '.dashboard_block',
                    columnWidth: 450,
                    
                    isResizable: true,
                    

                });
                /*jQuery('#dashboard-content').sortable({
                    distance: 12,
                    forcePlaceholderSize: true,
                    items: '.dashboard_block',
                    placeholder: 'dashboard_block_placeholder dashboard_block',
                    tolerance: 'pointer',
                    start: function(event, ui) {
                        ui.placeholder.height(ui.item.height());
                        ui.placeholder.width(ui.item.width());
                        ui.item.addClass('dashboard_dragging').removeClass('dashboard_block');
                        ui.item.parent().masonry('reload')
                    },
                    change: function(event, ui) {

                        ui.item.parent().masonry('reload');
                    },
                    stop: function(event, ui) {

                    },
                    update: function(event, ui) {
                        ui.item.removeClass('dashboard_dragging').addClass('dashboard_block');
                        ui.item.parent().masonry('reload');
                        var new_position = $('#dashboard-content').sortable("serialize", {key: "sort"});
                        
                    }
                });
                */
            });
        }
    }
})(jQuery);