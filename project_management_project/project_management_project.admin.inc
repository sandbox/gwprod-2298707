<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function project_management_project_admin_form($form, &$form_state)
{
    
    $default_settings = project_management_project_default_settings();
    $settings = array_replace_recursive($default_settings, variable_get('project_management_project_settings', array()));
    
    $form['submit'] = array(
        '#type' => 'submit',
        '#title' => t('Save')
    );
}
function project_management_project_admin_form_submit($form, &$form_state)
{
    variable_save('project_management_project_settings', $form_state['input']);
}
?>
