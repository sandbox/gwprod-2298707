<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function project_management_project_field_formatter_info() {
    return array(
        'text_list_with_override' => array(
            'label' => t('List Format with Name Override'),
            'field types' => array('text', 'text_long', 'text_with_summary'),
            'settings' => array(
                'project_management_project_label_override' => t('Default Label'),
            )
        )
    );
}

function project_management_project_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
    $element = array();
    $settings = $display['settings'];

    switch ($display['type']) {
        case 'text_list_with_override':
            // Common case: each value is displayed individually in a sub-element
            // keyed by delta. The field.tpl.php template specifies the markup
            // wrapping each value.
            $rows = array();
            watchdog('project_management_project_field_formatter_view', print_r($items, TRUE));
            if (!empty($items) || count($items) >= 0) {
                foreach ($items as $delta => $item) {
                    $rows[$delta] = array('data' => $item['value']);
                }
            } else {
                $rows[0] = array(
                    'data' => t('None')
                );
            }
            $element[0] = array(
                '#theme' => 'item_list',
                '#items' => $rows,
                '#type' => 'ul',
                '#attributes' => array()
            );
            break;
    }
    return $element;
}

function project_management_project_field_display_project_alter(&$display, $context) {
    if ($display['type'] == 'text_list_with_override') {
        //watchdog('project_management_project_field_display_project_alter', print_r($context, TRUE));
        //watchdog('project_management_project_field_display_project_alter', print_r($display, TRUE));
    }
}

function project_management_project_field_formatter_info_alter(&$info) {
    foreach ($info as $k => $formatter) {
        $info[$k]['settings']['project_management_project_label_override'] = '';
    }
}

function project_management_project_field_formatter_settings_summary($field, $instance, $view_mode) {
    $display = $instance['display'][$view_mode];
    $settings = $display['settings'];


    $summary = t('Label Override: @field', array('@field' => filter_xss($settings['project_management_project_label_override'], array())));


    return $summary;
}

function project_management_project_field_formatter_settings_summary_alter(&$summary, $context) {
    $display = $context['instance']['display'][$context['view_mode']];
    $settings = $display['settings'];

    if (!empty($summary)) {
        $summary .= '<br />';
    }

    if (!empty($settings['project_management_project_label_override'])) {
        $summary .= t('Label: @field', array('@field' => filter_xss($settings['project_management_project_label_override'], array())));
    } else {
        $summary .= t('Default label');
    }
}

function project_management_project_module_implements_alter(&$implementations, $hook) {
    // Make sure our hook_theme_registry_alter runs after field_display_label's.
    // This causes our preprocess_field hook to run after its one.
    if ($hook == 'theme_registry_alter') {
        $group = $implementations['project_management_project'];
        unset($implementations['project_management_project']);
        $implementations['project_management_project'] = $group;
    }
}

function project_management_project_theme_registry_alter(&$theme_registry) {
    // Move our hook_preprocess_field to the end of the array.
    $theme_registry['field']['preprocess functions'] = array_diff($theme_registry['field']['preprocess functions'], array('project_management_project_preprocess_field'));
    $theme_registry['field']['preprocess functions'][] = 'project_management_project_preprocess_field';
}

function project_management_project_preprocess_field(&$variables) {
    $field = field_info_instance($variables['element']['#entity_type'], $variables['element']['#field_name'], $variables['element']['#bundle']);
    //watchdog('project_management_project_preprocess_field', print_r($variables, TRUE));
    if (isset($field['display'][$variables['element']['#view_mode']]['settings']['project_management_project_label_override']) && strlen(trim($field['display'][$variables['element']['#view_mode']]['settings']['project_management_project_label_override'])) > 0) {
        $variables['label'] = $field['display'][$variables['element']['#view_mode']]['settings']['project_management_project_label_override'];
    }
}

?>
