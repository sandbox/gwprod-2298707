<?php
/**
 * @file
 * Default print module template
 *
 * @ingroup print
 */
//watchdog('print_template', print_r($content, TRUE));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Untitled Document</title>
        <style>
            *
            {
                margin:0;
                padding:0;
            }
            html {
                margin: 0.5in !important;
                padding: 0px !important;
            }
            html, body {
                background: none repeat scroll 0 0 #FFFFFF;
                font-size: 12pt;
                height: 100%;
                overflow: hidden;
                width: 7.5in;

            }
            .template {
                left: 0;
                top: 0;
                width: auto;
            }
            img {
                width: 100%;
            }
            li {
                margin-bottom:5px !important;
                margin-left:20px !important;
                margin-top:5px !important;
            }
            .activity_name {
                display:inline-block;
                width: 5in;
            }
            .date {
                display:inline-block;
                width: 2in;
            }
            .coordinators {
                display:block;
                width: 7in;
            }
            .header {
                border-bottom: 1px solid #000000;
                height: 1.2in;
                margin: 0;
                padding: 0;
            }
            .footer {
            }
            .budget {
                display:inline-block;
                width: 2in;
            }
            .participants {
                display:inline-block;
                width: 3in;
            }
            .hours_served {
                display:inline-block;
                width: 3in;
            }
            .content {
                height:7.5in;
                margin: 0;
                padding: 0;
            }
            .content .items {
                padding-top: 10px !important;
                text-decoration: underline;
            }

        </style>
    </head>

    <body>
        <div class="header">
            <h4>Center for Leadership and Community Engagement</h4>
            <h2>Executive Summary</h2>
            <p><span class="activity_name">Activity Name: <?php print $content->title; ?></span>
                <span class="date">Date: <?php print date('m-d-Y', $content->content['completion_date']); ?></span>
                <span class="coordinators">Coordinator(s): <?php print $content->content['coordinators']; ?></span></p>
        </div>
        <div class="content">
            <p class="items">Program Goals:<?php print render($content->content['goals']); ?></p>
            <p class="items">Measurable Learning Outcomes:<?php print render($content->content['learning_outcomes']); ?></p>
            <p class="items">Theories: <?php print render($content->content['theories']); ?></p>
            <p class="items">Action Tasks / Assignments: <?php print render($content->content['action_tasks']); ?></p>
            <p class="items">Road Blocks: <?php print render($content->content['roadblocks']); ?></p>
            <p class="items">Recommended Solutions: <?php print render($content->content['solutions']); ?></p>
            <?php
            if (isset($content->content['notes']) && $content->content['notes'] != '') {
                ?>
                <p class="items">Notes: <?php print render($content->content['notes']); ?></p>
            <?php } ?>
        </div>
        <div class="footer">
            <span class="budget">Budget: $<?php print render($content->content['total_spending']); ?></span>
            <span class="participants">No. of Participants: <?php print ($content->field_participants_approximate[LANGUAGE_NONE][0]['value']) ? '~' : ''; ?><?php print render($content->field_participants[LANGUAGE_NONE][0]['value']); ?></span>
            <span class="hours_served">No. of Hours Served: <?php print ($content->field_hours_served_approximate[LANGUAGE_NONE][0]['value']) ? '~' : ''; ?><?php print render($content->field_hours_served[LANGUAGE_NONE][0]['value']); ?></span>
        </div>
    </body>
</html>
