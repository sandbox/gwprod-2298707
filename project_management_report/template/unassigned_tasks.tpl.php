<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Untitled Document</title>
        <style>
            root {
                display: block;
            }
            task_completion {
            }
            span.task_completion {
                vertical-align: top;
            }
            span.task_users {
                display: inline-block;
                margin-left: 20px;
                vertical-align: top;
                width: 200px;
            }
            .task_date {
                display: inline-block;
                text-align: right;
                vertical-align: top;
                width: 200px;
            }
            body {
                background: none repeat scroll 0 0 transparent !important;
            }
            #page {
                background: none repeat scroll 0 0 transparent !important;
                font-family: verdana;
                font-weight: bold;
                text-align: left;
            }
            span.objective_item_summary {
                border: 1px solid black;
                display: block;
                margin-bottom: 10px;
                padding: 10px;
                width: 840px;
            }
            ul {
                font-size: 8px;
                list-style: none outside none;
            }
            .task {
                border: 1px solid black;
                margin-bottom: 10px;
                padding: 10px;
                width: 800px;
            }
            .task_description {
                display: inline-block;
                width: 297px;
            }
            ul li ul li {
                width: 822px;
            }
            ul li ul
            {
                list-style: none outside none;
            }

        </style>
    </head>

    <body>
        <div>
            <?php print render($content); ?>
        </div>
    </body>
</html>