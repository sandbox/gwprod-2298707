/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    Drupal.behaviors.officeHours = {
        attach: function (context, settings) {
            jQuery('.palette').live('click', function()
            {
                var currentSelection = jQuery('body').data(
                    'selection');
                if(typeof(currentSelection) != "undefined" && currentSelection 
                        !== null) {
                    
                    var data = new Object();
                    data.times = currentSelection;
                    data.setting = jQuery(this).attr('id');
                    jQuery.ajax({
                        type: 'POST',
                        url: Drupal.settings.project_management_schedule.URL,
                        dataType: 'json',
                        success: function(data) {
                            
                            jQuery.each(data.times, function(i, day){
                                
                                jQuery('#' + day.day_index + '-' + day.times)
                                    .removeClass();
                                jQuery('#' + day.day_index + '-' + day.times)
                                    .addClass(
                                    "office_hours_selected " + data.attribute);
                                jQuery('#' + day.day_index + '-' + day.times)
                                    .html(data.text);
                            });
                            
                            jQuery('#total').html("Total Hours: " + data.total);
                        },
                        data: data
                    });
                        
                    
                }
                
            });
            jQuery('#staffCal td').live('click',
                function() {
                    
                    var currentSelection = jQuery('body').data(
                        'selection');
                        
                    if(typeof(currentSelection) != "undefined" 
                        && currentSelection !== null) {
                        
                        if(!settings.alt && !settings.shifted)
                        {
                            for(var key in currentSelection)
                            {
                                
                                jQuery("#" + key)
                                    .removeClass('office_hours_selected');
                            }
                            jQuery('body')
                                .data('lastSelect', jQuery(this).attr('id'));
                            currentSelection = new Object();
                        } else if(settings.shifted)
{
                            var lastSelect = jQuery('body').data('lastSelect');
                            
                            var currentSelect = jQuery(this).attr('id');
                            
                            if(lastSelect && currentSelect)
                            {
                                var lastSelectParts = lastSelect.split('-');
                                var currentSelectParts = currentSelect.split('-');
                                var largestDayIndex = 
                                      Math.max(
                                            currentSelectParts[0],
                                            lastSelectParts[0]);
                                var smallestDayIndex = 
                                      Math.min(
                                            currentSelectParts[0],
                                            lastSelectParts[0]);
                                var largestTimeIndex = 
                                      Math.max(
                                            currentSelectParts[1],
                                            lastSelectParts[1]);
                                var smallestTimeIndex = 
                                      Math.min(
                                            currentSelectParts[1],
                                            lastSelectParts[1]);
                                ;
                                
                                for(
                                    var i = smallestDayIndex; 
                                    i <= largestDayIndex; 
                                    i++) {
                                        
                                    for(
                                        var j = smallestTimeIndex; 
                                        j <= largestTimeIndex; 
                                        j++) {
                                        
                                        if(!(currentSelectParts[0] == i 
                                            && currentSelectParts[1] == j))
                                        {
                                            currentSelection[i+'-' +j] = true;
                                            jQuery("#" + i+'-' +j)
                                                .addClass('office_hours_selected');
                                            
                                        }
                                    }
                                }
                            }
                            else {
                                jQuery('body')
                                    .data('lastSelect', jQuery(this).attr('id'));
                            }
                                
                        } else if(settings.alt)
{
                            jQuery('body')
                                .data('lastSelect', jQuery(this).attr('id'));
                        }
                    } else {
                        currentSelection = new Object();
                        jQuery('body')
                            .data('lastSelect', jQuery(this).attr('id'));
                    }
                    if(currentSelection[jQuery(this).attr('id')])
                    {
                        delete currentSelection[jQuery(this).attr('id')];
                        jQuery(this)
                            .removeClass('office_hours_selected');
                    } else {
                        currentSelection[jQuery(this).attr('id')] = true;
                        jQuery(this)
                            .addClass('office_hours_selected');
                    }
                    
                    
                    jQuery('body').data(
                        'selection', currentSelection);
                    
                });
            jQuery(document).bind('keyup keydown', function(e){
                
                settings.shifted = e.shiftKey;
                settings.ctrl = e.ctrlKey;
                settings.alt = e.altKey;
               
                
            } );
            
        }
    }
})(jQuery);


