/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


(function ($) {
    Drupal.behaviors.schedules = {
        attach: function (context, settings) {
    
            jQuery('.staff_list_element').live('click',
                function() {

                    if(jQuery('body').data(jQuery(this).attr('id')) == 1)
                    {
                        jQuery(this).removeAttr('style');
                        jQuery('body').data(jQuery(this).attr('id'), 0);
                        jQuery('#staff_total').html("Staff Hours: 0");
                    } else {
                        jQuery.ajax({
                            type: 'POST',
                            url: 'users/schedules',
                            dataType: 'json',
                            success: function(data) {
                                jQuery('#staff_total')
                                    .html("Staff Hours: " + data.time);
                            },
                            data: 'user=' + jQuery(this).attr('id')
                        });
                        jQuery(this).css('background-color','#FFDADA');
                        jQuery('body').data(jQuery(this).attr('id'), 1);
                    }
                    var elements = '';
                    jQuery('#staffCal').find('span').removeAttr('style');
                    jQuery.each( jQuery('body').data(),function(i, e) {
                        if(e == 1)
                        {
                            elements += '.' + i;
                            var color = userIDtoColor(i);
                            var textcolor = 
                                (parseInt('FFFFFF', 16) - parseInt(color, 16))
                                    .toString(16)
                            
                            jQuery('.n' + i).css('background-color', '#' + color);
                            jQuery('.n' + i).css('color', '#FFFFFF');
                        }
                    });
                    jQuery('#staffCal').find('td').removeAttr('style');
                    jQuery(elements).css('background-color','#FFDADA');
                });
        }
    }
})(jQuery);
function userIDtoColor(userID)
{
    var modifiedNumber = userID * 1212121;
    var hexString = modifiedNumber.toString(16);
    return hexString.substring(0,6);
}
