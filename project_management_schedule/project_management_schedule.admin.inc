<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function project_management_schedule_admin_form($form, &$form_state) {
    $settings = project_management_schedule_get_settings();
    /*$form['error'] = array(
        '#markup' => print_r($settings, TRUE)
    );*/
    
    
    $form['times'] = array(
        '#type' => 'fieldset',
        '#title' => t('Time Configuration'),
        '#tree' => true,
        '#collapsible' => true,
        '#collapsed' => true
    );
    $form['times']['start_time'] = array(
        '#title' => t('Begin Display'),
        '#type' => 'textfield',
        '#default_value' => date('h:i a', $settings['times']['start_time'])
    );
    $form['times']['end_time'] = array(
        '#title' => t('End Display'),
        '#type' => 'textfield',
        '#default_value' => date('h:i a', $settings['times']['end_time'])
    );
    $form['user_fields'] = array(
        '#type' => 'fieldset',
        '#title' => t('Naming Configuration.'),
        '#tree' => true,
        '#collapsible' => true,
        '#collapsed' => true
    );
    $instances = field_info_instances('user', 'user');
    foreach ($instances as $key => $instance) {
        $form['user_fields'][$key] = array(
            '#type' => 'checkbox',
            '#title' => check_plain($instance['label']),
            '#default_value' => (isset($settings['user_fields'][$key]) && $settings['user_fields'][$key] == $key),
            '#return_value' => $key
        );
    }
    $form['menu_items'] = array(
        '#type' => 'fieldset',
        '#title' => t('Menu Item Names.'),
        '#tree' => true,
        '#collapsible' => true,
        '#collapsed' => true
    );
    $form['menu_items']['change_office_hours'] = array(
        '#type' => 'textfield',
        '#title' => t('Name to display as Tab on User Profile'),
        '#default_value' => $settings['menu_items']['change_office_hours']
    );
    $form['menu_items']['staff_work_schedules'] = array(
        '#type' => 'textfield',
        '#title' => t('Name to display For Staff Work Schedules'),
        '#default_value' => $settings['menu_items']['staff_work_schedules']
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit')
    );
    return $form;
}

function project_management_schedule_admin_form_validate($form, &$form_state) {
    
}

function project_management_schedule_admin_form_submit($form, &$form_state) {

    print_r($form_state['input']);
    $variables = $form_state['input'];
    $variables['times']['start_time'] = strtotime($variables['times']['start_time']);
    $variables['times']['end_time'] = strtotime($variables['times']['end_time']);
    $settings = project_management_schedule_get_settings();
    variable_set('project_management_schedule_settings', array_replace_recursive($settings, $variables));
    
    $form_state['rebuild'] = true;
}

?>
