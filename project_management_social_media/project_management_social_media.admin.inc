<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function project_management_social_media_admin_form($form, &$form_state) {
    $settings = project_management_social_media_settings();
    $form['social_media_settings'] = array(
        '#tree' => TRUE
    );
    $form['social_media_settings']['fb'] = array(
        '#tree' => true
    );
    $form['social_media_settings']['fb']['app_id'] = array(
        '#title' => t('Facebook App ID'),
        '#type' => 'textfield',
        '#default_value' => isset($settings['fb']['app_id']) ? $settings['fb']['app_id'] : ''
    );
    $form['social_media_settings']['fb']['app_secret'] = array(
        '#title' => t('Facebook App Secret'),
        '#type' => 'textfield',
        '#default_value' => isset($settings['fb']['app_secret']) ? $settings['fb']['app_secret'] : ''
    );
    if (isset($settings['fb']['page_access_token'])) {
        $form['social_media_settings']['fb']['page_access_token_display'] = array(
            '#title' => t('Page Access Token'),
            '#type' => 'item',
            '#markup' => isset($settings['fb']['page_access_token']) ? l($settings['fb']['page_access_token'], 'https://developers.facebook.com/tools/debug/access_token?q=' . $settings['fb']['page_access_token']) : ''
        );
        $form['social_media_settings']['fb']['page_access_token'] = array(
            '#title' => t('Page Access Token'),
            '#type' => 'hidden',
            '#value' => isset($settings['fb']['page_access_token']) ? $settings['fb']['page_access_token'] : ''
        );
    }

    $form['social_media_settings']['fb']['page_id'] = array(
        '#title' => t('Page ID'),
        '#type' => 'textfield',
        '#maxlength' => 255,
        '#default_value' => isset($settings['fb']['page_id']) ? $settings['fb']['page_id'] : ''
    );
    //

    return system_settings_form($form);
}

function project_management_social_media_get_token_form($form, &$form_state, $access_token = null, $expires = null) {
    if (isset($access_token)) {
        $form['access_token'] = array(
            '#type' => 'hidden',
            '#value' => $access_token
        );
        $form['access_token_display'] = array(
            '#type' => 'item',
            '#title' => t('Access Token'),
            '#markup' => $access_token
        );
        $form['expires'] = array(
            '#type' => 'hidden',
            '#value' => $expires
        );
        $form['expires_display'] = array(
            '#type' => 'item',
            '#title' => t('Expires'),
            '#markup' => $expires
        );
    }
    $form['permissions'] = array(
        '#type' => 'checkboxes',
        '#options' => array(
            'create_note' => 'Create Note',
            'manage_pages' => 'Manage Pages',
            'photo_upload' => 'Upload Photos',
            'publish_actions' => 'Publish Actions',
            'publish_stream' => 'Publish to Stream',
            'share_item' => 'Share Item',
            'create_event' => 'Create Event',
            'status_update' => 'Update Status',
            'video_upload' => 'Upload Video',
            'read_stream' => 'Read Stream'
        ),
        '#default_value' => array(
            'create_note',
            'manage_pages',
            'photo_upload',
            'publish_actions',
            'publish_stream',
            'share_item',
            'create_event',
            'status_update',
            'video_upload',
            'read_stream'
        )
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Get Personal Access Token')
    );
    return $form;
}

function project_management_social_media_get_token_form_submit($form, &$form_state) {
    $permissions = implode(',', array_keys($form_state['values']['permissions']));
    //drupal_set_message(print_r($form_state['values']['permissions'], TRUE));
    project_management_social_media_get_user_token($permissions);
}

function project_management_social_media_monitored_pages() {
    $settings = project_management_social_media_settings();
    $rows = array();
    if (isset($settings['monitored_pages'])) {
        foreach ($settings['monitored_pages'] as $page_id => $page) {
            $rows[$page_id] = array(
                'page_name' => l($page['name'], 'admin/structure/project_social_media/monitored_pages/' . $page_id),
                'page_id' => $page_id
            );
        }
    }
    $header = array(t('Page Name'), t('Page ID'));
    $content['create_page'] = array(
        '#theme' => 'link',
        '#text' => t('Add Page for Monitoring'),
        '#path' => 'admin/structure/project_social_media/monitored_pages/new',
        '#options' => array(
            'attributes' => array(),
            'html' => FALSE
        )
    );
    $content['pages'] = array(
        '#theme' => 'table',
        '#rows' => $rows,
        '#empty' => t('No Pages are currently being monitored'),
        '#header' => $header,
    );
    return $content;
}

function project_management_social_media_monitored_page_form($form, &$form_state, $page_id = null) {
    $form['page_id'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($page_id) ? $page_id : '',
        '#title' => t('Facebook Page ID'),
        '#description' => t('Facebook Page ID to monitor')
    );
    if (isset($page_id)) {
        $settings = project_management_social_media_settings();
        $page = $settings['monitored_pages'][$page_id];
        $form['page_name'] = array(
            '#type' => 'item',
            '#markup' => $page['about']
        );
    }
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => isset($page_id) ? t('Update') : t('Add Page')
    );


    return $form;
}

function project_management_social_media_monitored_page_form_submit($form, &$form_state) {
    $settings = project_management_social_media_settings();
    $page = project_management_social_media_get_page($form_state['values']['page_id']);
    $settings['monitored_pages'][$form_state['values']['page_id']] = $page;
    variable_set('social_media_settings', $settings);
    $form_state['redirect'] = 'admin/structure/project_social_media/monitored_pages';
}





?>
