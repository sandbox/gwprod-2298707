<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$totalHours = 0;
$totalInstances = 0;
$totalIndividuals = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Untitled Document</title>
        <style>
            *
            {
                margin:0;
                padding:0;
            }
            html {
                margin: 0.5in !important;
                padding: 0px !important;
            }
            html, body {
                background:none repeat scroll 0 0 #FFFFFF;
                font-size:12pt;
                height:100%;
                overflow:hidden;
                width:7.5in;
            }
            .template {
                left: 0;
                top: 0;
                width: auto;
            }
            img {
                width: 100%;
            }
            li {
                margin-bottom:5px !important;
                margin-left:20px !important;
                margin-top:5px !important;
            }
            .activity_name {
                display:inline-block;
                width: 5in;
            }
            .date {
                display:inline-block;
                width: 2in;
            }
            .coordinators {
                display:block;
                width: 7in;
            }
            .header {
                border-bottom: 1px solid #000000;
                height: 1.2in;
                margin: 0;
                padding: 0;
            }
            .footer {
            }
            .budget {
                display:inline-block;
                width: 2in;
            }
            .participants {
                display:inline-block;
                width: 3in;
            }
            .hours_served {
                display:inline-block;
                width: 3in;
            }
            .content {
                height:8in;
                margin: 0;
                padding: 0;
            }
            .content .items {
                padding-top: 10px !important;
                text-decoration: underline;
            }
            .right
            {
                text-align:right;
            }
            .vertical_header
            {
                text-align:left;

            }
            #statisticsTable
            {
                width:100%;
            }
            th, td {
                border:1px dotted black;
                padding:4px;
            }
            td.left
            {
                text-align:right;
            }
        </style>
    </head>

    <body>

        <?php print $statistics->content['#content']; ?>
    </body>
</html>