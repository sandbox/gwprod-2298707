<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class ='task-details'>
    <div class="task_description">
        <span>Task Description:</span>
    <?php print $content['description'];?>
    </div>
<div class='task-details-item'><span class="task-details-item-label">Deadline:</span><?php print $content['due_date'];?></div>
<div class='task-details-item'><span class="task-details-item-label">Status:</span><?php print $content['status'];?></div>
<div class='task-details-item'><span class="task-details-item-label">Priority:</span><?php print $content['priority'];?></div>
<div class='task-details-item'><span class="task-details-item-label">Volunteer Doable:</span><?php print $content['volunteer_doable'];?></div>
<div class='attached_data'><?php print render($content['attached_data']);?></div>
</div>