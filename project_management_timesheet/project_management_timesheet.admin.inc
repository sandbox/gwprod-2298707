<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function project_management_timesheet_admin_form($form, &$form_state)
{
  $settings = variable_get('project_management_timesheet_settings');
  $form['employer_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Employer Name'),
    '#default_value' => isset($settings['employer_name']) ? $settings['employer_name'] : ''
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  return $form;
}
function project_management_timesheet_admin_form_submit($form, &$form_state)
{
  variable_set('project_management_timesheet_settings', $form_state['values']);
  drupal_set_message('Timesheet Settings Saved.');
}
?>
