<?php
/**
 * @file
 * Default print module template
 *
 * @ingroup print
 */
//watchdog('print_template', print_r($content, TRUE));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Untitled Document</title>
        <style>
            * {
                margin: 0;
                padding: 0;
            }
            html {
                margin: 0.25in !important;
                padding: 0 !important;
            }

            body {
                background: none repeat scroll 0 0 transparent;
                font: 9px Arial,'Liberation Sans',FreeSans,sans-serif;

                padding: 0;
                width: 8in;
            }
            #page {
                background: none repeat scroll 0 0 transparent;
            }
            tbody {
                border-top: 0 none;
            }
            table {
                font: 12px Arial,'Liberation Sans',FreeSans,sans-serif;
                margin: 0 !important;
                width: 100%;
            }
            #topTable {
                padding: 4px 6px 0;
            }
            #topTable td {
                height: 24px;
                vertical-align: top;
            }
            h2 {
                color: black;
            }
            div.content {
                padding: 10px;
                text-align: left;
            }
            #container {
                font-family: Georgia;
                text-align: left;

            }
            #title {
                font-size: 16px;
                font-weight: bold;
            }
            .uline {
                border-bottom: 1px solid #000000;
                font: 11px 'Times New Roman','Liberation Sans',FreeSans,sans-serif;
            }
            .nouline {
                border-bottom: medium none;
            }
            .floatLeft {
                display: inline-block;
            }
            .floatRight {
                float: right;
            }
            .label {
                display:inline-block;
                font: 11px Arial,'Liberation Sans',FreeSans,sans-serif;
                margin-right: 6px;
                width: 95px;
            }
            #dep {
                width: 2.8in;
            }
            #name {
                width: 2in;
                margin-right:1in;
            }
            #pay {
                font-size: 13px;
                width: 2in;
                margin-right:1in;
            }
            #cwu {
                width: 2.8in;
            }
            #optionsTable {
                -moz-border-bottom-colors: none;
                -moz-border-image: none;
                -moz-border-left-colors: none;
                -moz-border-right-colors: none;
                -moz-border-top-colors: none;
                border-collapse: collapse;
                border-color: #000000 #000000 -moz-use-text-color;
                border-style: solid solid none;
                border-width: 1px 1px medium;
            }
            #optionsTable td {
                border-bottom: 1px solid black;
                height: 20px;
            }
            #optionsTable .nouline td {
                border-bottom: medium none;
                padding: 2px;
                vertical-align: top;
            }
            .checkBox {
                border: 1px solid black;
                display: inline-block;
                height: 8px;
                margin: 2px;
                vertical-align: top;
                width: 8px;
            }
            #yes {
                display:inline-block;
                width: 50%;
            }
            #no {
                display:inline-block;
                width: 50%;
            }
            #timeTable {
                border-collapse: collapse;
                border-top: 2px solid #000000;
                margin-top: 4px;
            }
            #timeTable td {
                border: 1px solid #000000;
                height: 20px;
                text-align: center;
                width: 61px;
            }
            #timeTable .top td {
                border-bottom: 2px solid #000000;
                height: 40px;
                padding-top: 18px;
            }
            #timeTable #cStaff {
                margin: 0;
                padding: 0;
            }
            #cStaff td {
                border: medium none;
            }
            #cStaff table {
                border-collapse: collapse;
                font-size: 8px;
            }
            #cStaff #head {
                border-bottom: 1px solid #000000;
                height: 12px;
                padding-top: 2px;
            }
            #cStaff #cStaffLeft {
                border-bottom: medium none;
                border-right: 1px dashed #000000;
                padding-top: 0;
            }
            #cStaff #cStaffRight {
                border-bottom: medium none;
                padding-top: 0;
            }
            #timeTable .balance {
                padding-right: 5px;
                text-align: right;
            }
            #timeTable .cStaffLeft {
                border-right: 1px dashed #000000;
                padding-top: 0;
                width: 70px;
            }
            #timeTable .cStaffRight {
                border-left: medium none;
                padding-top: 0;
            }
            #timeTable .payPeriod {
                padding-right: 5px;
                text-align: right;
            }
            #timeTable .disclaimer {
                font-size: 6pt;
                height: 10px;
            }
            #bottomBox {
                border: 1px solid #000000;
                margin-top: 4px;
            }
            #bottomBox .ifyes {
                border-bottom: 1px solid #000000;
            }
            #bottomBox .note {
                font-size: 5pt;
            }
            #lastTable {
                font-size: 6pt;
            }
            #lastTable .uline .blank {
                border: medium none;
                width: 20px;
            }
            #lastTable .uline td {
                border-bottom: 1px solid #000000;
            }
            .right {
                text-align: right;
            }
            #ifyou {
                font-family: Verdana;
                font-size: 8pt;
                margin: 4px 10px 0;
                text-align: left;
            }
            #original {
                font-size: 5pt;
                margin-top: 4px;
            }
            #logs {
                font: 12pt Arial,'Liberation Sans',FreeSans,sans-serif;
                
            }
            #logs table {
                font: 10pt Arial,'Liberation Sans',FreeSans,sans-serif;
                margin: 0 0 12px 40px !important;
                width: 400px;
            }
            #logs table th {
                font-weight: bold;
                text-align: left;
                vertical-align: top;
                width: 100px;
            }
            tr, tr td, tr th {
                background: none repeat scroll 0 0 transparent !important;
                border-color: white;
                color: #000000 !important;
                padding: 0 !important;
            }
            div#reminder {
                font-size: small;
                font-weight: bold;
                padding-top: 10px;
                page-break-before: always;
            }
            #header
            {
                text-align:center;
            }

        </style>
    </head>

    <body>
        <?php print $content; ?>
    </body>
</html>