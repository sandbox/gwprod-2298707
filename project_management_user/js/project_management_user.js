/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


(function ($) {
    
    
    Drupal.behaviors.dragAndDrop = {
        attach: function (context, settings) {
    

            
            jQuery( ".connectedSortable" ).droppable({
                
                tolerance: "touch",
                activeClass: "drop-hover"
            });
            jQuery( "#available_list, #coordinator_list, #approved_list, #unapproved_list" ).sortable({
                connectWith: ".connectedSortable",
        
                receive: function(event, ui) { 
                    
                    var serial = jQuery(this).sortable('serialize');
                    
                    serial +="&caller=" + jQuery(this).attr('id');
                    
                    jQuery.ajax({
                        type: "POST",
                        url: Drupal.settings.project_management_user.user_URL,
                        data: serial,
                        success: processDropSuccess
                    });
            
            

                }
            });
            jQuery(document).undelegate("ul.ui-sortable li a.user_remove", "click", clickToRemove); 
            jQuery(document).delegate("ul.ui-sortable li a.user_remove", "click", clickToRemove); 
        }
    }
})(jQuery);
    

var draggable = new Drupal.ajax("#coordinator_list", jQuery("#coordinator_list"), {
    url: '', 
    event:'click'
});
function processDropSuccess(data)
{
    
    
    draggable.success(data);
    
}
function clickToRemove()
{
    
    var id = jQuery(this).parents("li").attr('id');
    var element = this;
    if(id == 'user_' + Drupal.settings.project_management_user.logged_in_user_id)
    {
        jQuery( "#dialog-confirm-remove" ).dialog({
            resizable: false,
            height:180,
            modal: true,
            buttons: {
                "Remove User": function() {
                    
                    jQuery( this ).dialog( "close" );
                    
                    removeUser(id, element);
                },
                Cancel: function() {
                    jQuery( this ).dialog( "close" );
                }
            }
        });
    } else {
        removeUser(id, element);
    }
    
    return false;
            
}
function removeUser(id, element)
{
    
    var dataObject = {};
    dataObject.id=id;
    jQuery.ajax({
        type: "POST",
        url: Drupal.settings.project_management_user.user_remove_URL,
        data: dataObject,
        success: processDropSuccess
    });
    
    jQuery(element).parents("li").remove();
    return false;
}
