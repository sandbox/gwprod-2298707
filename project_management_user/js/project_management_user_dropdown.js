/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


(function ($) {
    
    Drupal.behaviors.menuDropDown = {
        attach: function (context, settings)
        {
        
            
            $('#project_management_user_logged_in_block li:has(ul)').hover(
                function () {
                    $(this).find('a:first').addClass('hovering');
                    var margin = (($(this).width() - $('div.dropdown', this).width())/2)+1;
                    
                    $('div.dropdown', this).css('margin-left', margin);
                    $('div.dropdown', this).stop().slideDown(0);
 
                }, 
                function () {
                    $(this).find('a:first').removeClass('hovering');
                    $('div.dropdown', this).stop().slideUp(0);          
                }
                );
     

        }
    }
})(jQuery);