<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function project_management_user_admin_form($form, &$form_state) {
  $variables = variable_get('project_management_user');
  //print_r($variables);
  $instances = field_info_instances('user', 'user');

  $form['override_user_view'] = array(
    '#type' => 'checkbox',
    '#default_value' => isset($variables['override_user_view']) ? $variables['override_user_view'] : 0,
    '#title' => t('Override the default drupal user view?'),
  );
  $form['roles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Roles'),
    '#description' => t('Roles that can be select for users.'),
    '#tree' => true
  );
  $roles = user_roles(true);
  foreach ($roles as $role_id => $role_name) {
    $form["roles"][$role_id] = array(
      '#type' => 'checkbox',
      '#title' => check_plain($role_name),
      '#value' => (isset($variables['roles'][$role_id]) && $variables['roles'][$role_id] == 1)
    );
  }

  $form['user_fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('User Fields'),
    '#description' => t('User fields to search against.  These fields are concatenated for search and display.'),
    '#tree' => true
  );
  foreach ($instances as $key => $instance) {
    $form["user_fields"][$key] = array(
      '#type' => 'checkbox',
      '#title' => check_plain($instance['label']),
      '#value' => (isset($variables['user_fields'][$key]) && $variables['user_fields'][$key] == 1)
    );
  }
  $form['contact_fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Contact Fields'),
    '#description' => t('User fields to request updates from user.'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
  foreach ($instances as $key => $instance) {
    $form['contact_fields'][$key] = array(
      '#type' => 'checkbox',
      '#title' => check_plain($instance['label']),
      '#value' => (isset($variables['contact_fields'][$key]) && $variables['contact_fields'][$key] == 1)
    );
  }
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  return $form;
}

function project_management_user_admin_form_submit($form, &$form_state) {
  $save_array = array();
  variable_del('project_management_user');
  //watchdog('project_management_user_admin_form_submit', print_r($save_array, TRUE));
  variable_set('project_management_user', array(
    'user_fields' => $form_state['input']['user_fields'],
    'contact_fields' => $form_state['input']['contact_fields'],
    'roles' => $form_state['input']['roles'],
    'override_user_view' => $form_state['values']['override_user_view'])
  );
  $form_state['rebuild'] = true;
}

?>
